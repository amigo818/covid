<!DOCTYPE html>
<html  lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/  dc: http://purl.org/dc/terms/  foaf: http://xmlns.com/foaf/0.1/  og: http://ogp.me/ns#  rdfs: http://www.w3.org/2000/01/rdf-schema#  schema: http://schema.org/  sioc: http://rdfs.org/sioc/ns#  sioct: http://rdfs.org/sioc/types#  skos: http://www.w3.org/2004/02/skos/core#  xsd: http://www.w3.org/2001/XMLSchema# ">
  <head>
    <meta charset="utf-8" />
<link rel="canonical" href="https://www.fda.gov/" />
<link rel="shortlink" href="https://www.fda.gov/" />
<meta name="description" content="The FDA is responsible for protecting the public health by ensuring the safety, efficacy, and security of human and veterinary drugs, biological products, and medical devices; and by ensuring the safety of our nation&#039;s food supply, cosmetics, and products that products that emit radiation." />
<meta name="dcterms.title" content="U.S. Food and Drug Administration" />
<meta name="dcterms.creator" content="Office of the Commissioner" />
<meta name="dcterms.description" content="The FDA is responsible for protecting the public health by ensuring the safety, efficacy, and security of human and veterinary drugs, biological products, and medical devices; and by ensuring the safety of our nation&#039;s food supply, cosmetics, and products that products that emit radiation." />
<meta name="dcterms.publisher" content="FDA" />
<meta name="dcterms.source" content="FDA" />
<meta property="og:site_name" content="U.S. Food and Drug Administration" />
<meta property="og:type" content="website" />
<meta property="og:url" content="https://www.fda.gov/home" />
<meta property="og:title" content="U.S. Food and Drug Administration" />
<meta property="og:description" content="The FDA is responsible for protecting and promoting the public health." />
<meta property="og:image" content="https://www.fda.gov/themes/custom/preview/img/FDA-Social-Graphic.png" />
<meta property="og:updated_time" content="Wed, 09/22/2021 - 19:30" />
<meta property="article:published_time" content="Wed, 09/22/2021 - 19:30" />
<meta property="article:modified_time" content="Wed, 09/22/2021 - 19:30" />
<meta name="google-site-verification" content="tWxlDhm4ANdksJZPj7TBmHgNoMqZCnecPp0Aa2vC9XA" />
<meta name="twitter:card" content="summary_large_image" />
<meta property="twitter:description" content="The FDA is responsible for protecting and promoting the public health." />
<meta name="twitter:site" content="@US_FDA" />
<meta name="twitter:title" content="U.S. Food and Drug Administration" />
<meta name="twitter:creator" content="@US_FDA" />
<meta name="twitter:image" content="https://www.fda.gov/themes/custom/preview/img/FDA-Social-Graphic.png" />
<meta name="MobileOptimized" content="width" />
<meta name="HandheldFriendly" content="true" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="shortcut icon" href="/themes/custom/preview/favicon.ico" type="image/vnd.microsoft.icon" />
<link rel="alternate" hreflang="en" href="https://www.fda.gov/" />
<link rel="revision" href="https://www.fda.gov/home" />

    <title>COVID-19 Vaccination</title>
    <link rel="stylesheet" media="all" href="/files/css/css_HtELcYsO8Y9V55ZucaaUfbJJl3XP-oMN-B-51QHIhA4.css?v=34" />
<link rel="stylesheet" media="all" href="/files/css/css_UHlph9qVFtxeqVYAtGfS7bo79NZ1HLTRMesJqOtoM9E.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker3.min.css">

  </head>
  <body class="role-anonymous path-frontpage page-node-type-section has-glyphicons">
      <div id="quicklinks" class="sr-only">
        <ul>
          <li><a href="#main-content" class="sr-only sr-only-focusable" tabindex="1">Skip to main content</a></li>
          <li><a href="#search-form" class="sr-only sr-only-focusable" tabindex="1">Skip to FDA Search</a></li>
          <li><a href="#section-nav" class="sr-only sr-only-focusable" tabindex="1">Skip to in this section menu</a></li>
          <li><a href="#footer-heading" class="sr-only sr-only-focusable" tabindex="1">Skip to footer links</a></li>
        </ul>
      </div>
      <div class="dialog-off-canvas-main-canvas" data-off-canvas-main-canvas>


  <div class="main-container container-fluid">    <div class="row">


<div class="col-xs-12">
    <div data-drupal-messages-fallback class="hidden"></div>

</div>



    <header class="lcds-header container-fluid" role="header">

            <div class="row fda-masthead">
                            <div class="col-xs-4 col-md-8">
                    <a href="/" title="FDA Homepage">
                        <h1 class="fda-masthead-2">COVID-19 Vaccination</h1>
                    </a>
        </div>

        <div class="col-xs-8 col-md-4">
          <ul class='fda-masthead__item-list'>
            <li><a title="" id="btn-search" class="btn btn-default btn-sm fda-masthead__btn-search">
              <span class="fa fa-search" aria-hidden="true">&nbsp;</span>
              <span class="fda-masthead__btn-label">Search</span>
            </a></li>
            <li><a id="menu-btn" class="btn btn-default btn-sm fda-masthead__btn-menu collapsed" href="#primary-nav" data-toggle="collapse" aria-expanded="true">
              <span class="fa fa-bars" aria-hidden="true">&nbsp;</span>
              <span class='fda-masthead__btn-label'>Menu</span>
            </a></li>
          </ul>
                </div>

                        <form class="fda-masthead__search sr-only" role="search" action="https://search.usa.gov/search" method="GET" name="searchForm" id="search-form" accept-charset="UTF-8" >

                            <div class="search-popover" id="search-popover">
                                <div class="input-group pull-right" id="search-group">
                                    <label class="sr-only" for="search-query">Search FDA</label>

                                    <input accesskey="4" class="form-control search-input" id="search-query" name="query" aria-autocomplete="list" aria-haspopup="true" title="Enter the terms you wish to search for." placeholder="Search FDA"  type="text" />

                                    <span class="input-group-btn" id="input-group-btn">

                                        <button type="submit" class="btn btn-danger search-btn" id="search-btn" title="Search"><span class="fa fa-search" aria-hidden="true"><span class="sr-only">Submit search</span></span></button>

                                    </span>

                                </div>
                            </div>

                            <input name="affiliate" value="fda1" type="hidden">
                        </form>

            </div>
        <nav id="primary-nav" class="lcds-primary-nav row collapse">                    <div class="col-md-5 col-lg-4">
                        <section class="lcds-primary-nav__group lcds-primary-nav__group--bordered">
                            <h2 class="lcds-primary-nav__group-heading">Featured</h2>
                            <ul class="lcds-primary-nav__list lcds-primary-nav__list--featured">
                    <li class="lcds-primary-nav__list-item"><a href="https://www.fda.gov/about-fda/contact-fda">Contact FDA</a></li>
                    <li class="lcds-primary-nav__list-item"><a href="https://www.fda.gov/regulatory-information/search-fda-guidance-documents">FDA Guidance Documents</a></li>
                    <li class="lcds-primary-nav__list-item"><a href="https://www.fda.gov/safety/recalls-market-withdrawals-safety-alerts">Recalls, Market Withdrawals and Safety Alerts</a></li>
                    <li class="lcds-primary-nav__list-item"><a href="https://www.fda.gov/news-events/newsroom/press-announcements">Press Announcements</a></li>
                    <li class="lcds-primary-nav__list-item"><a href="https://www.fda.gov/inspections-compliance-enforcement-and-criminal-investigations/compliance-actions-and-activities/warning-letters">Warning Letters</a></li>
                    <li class="lcds-primary-nav__list-item"><a href="https://www.fda.gov/advisory-committees">Advisory Committees</a></li>
                    <li class="lcds-primary-nav__list-item"><a href="https://www.fda.gov/about-fda/en-espanol">En Espa&#241ol</a></li>
                            </ul>
                        </section>
                    </div>

                    <div class="col-md-7 col-lg-8">
          <section class="lcds-primary-nav__group lcds-primary-nav__group--bordered">
            <h2 class="lcds-primary-nav__group-heading">Products</h2>
            <ul class="lcds-primary-nav__list">
                            <li class="lcds-primary-nav__list-item"><a href="https://www.fda.gov/food">Food</a></li>
                            <li class="lcds-primary-nav__list-item"><a href="https://www.fda.gov/drugs">Drugs</a></li>
                            <li class="lcds-primary-nav__list-item"><a href="https://www.fda.gov/medical-devices">Medical Devices</a></li>
                            <li class="lcds-primary-nav__list-item"><a href="https://www.fda.gov/radiation-emitting-products">Radiation-Emitting Products</a></li>
                            <li class="lcds-primary-nav__list-item"><a href="https://www.fda.gov/vaccines-blood-biologics">Vaccines, Blood, and Biologics</a></li>
                            <li class="lcds-primary-nav__list-item"><a href="https://www.fda.gov/animal-veterinary">Animal and Veterinary</a></li>
                            <li class="lcds-primary-nav__list-item"><a href="https://www.fda.gov/cosmetics">Cosmetics</a></li>
                            <li class="lcds-primary-nav__list-item"><a href="https://www.fda.gov/tobacco-products">Tobacco Products</a></li>
            </ul>
          </section>
          <section class="lcds-primary-nav__group lcds-primary-nav__group--bordered">
            <h2 class="lcds-primary-nav__group-heading">Topics</h2>
            <ul class="lcds-primary-nav__list">
                            <li class="lcds-primary-nav__list-item"><a href="https://www.fda.gov/about-fda">About FDA</a></li>
                            <li class="lcds-primary-nav__list-item"><a href="https://www.fda.gov/combination-products">Combination Products</a></li>
                            <li class="lcds-primary-nav__list-item"><a href="https://www.fda.gov/regulatory-information">Regulatory Information</a></li>
                            <li class="lcds-primary-nav__list-item"><a href="https://www.fda.gov/safety">Safety</a></li>
                            <li class="lcds-primary-nav__list-item"><a href="https://www.fda.gov/emergency-preparedness-and-response">Emergency Preparedness</a></li>
                            <li class="lcds-primary-nav__list-item"><a href="https://www.fda.gov/international-programs">International Programs</a></li>
                            <li class="lcds-primary-nav__list-item"><a href="https://www.fda.gov/news-events">News and Events</a></li>
                            <li class="lcds-primary-nav__list-item"><a href="https://www.fda.gov/training-and-continuing-education">Training and Continuing Education</a></li>
                            <li class="lcds-primary-nav__list-item"><a href="https://www.fda.gov/inspections-compliance-enforcement-and-criminal-investigations">Inspections and Compliance</a></li>
                            <li class="lcds-primary-nav__list-item"><a href="https://www.fda.gov/science-research">Science and Research</a></li>
            </ul>
          </section>
          <section class="lcds-primary-nav__group lcds-primary-nav__group--bordered">
            <h2 class="lcds-primary-nav__group-heading">Information For</h2>
            <ul class="lcds-primary-nav__list">
                <li class="lcds-primary-nav__list-item"><a href="https://www.fda.gov/consumers">Consumers</a></li>
                <li class="lcds-primary-nav__list-item"><a href="https://www.fda.gov/patients">Patients</a></li>
                <li class="lcds-primary-nav__list-item"><a href="https://www.fda.gov/industry">Industry</a></li>
                <li class="lcds-primary-nav__list-item"><a href="https://www.fda.gov/health-professionals">Health Professionals</a></li>
                <li class="lcds-primary-nav__list-item"><a href="https://www.fda.gov/federal-state-local-tribal-and-territorial-officials">Federal, State and Local Officials</a></li>
            </ul>
          </section>
                    </div>
          </nav>
    </header>



                       <main>

                          <div id="main-content">







  <div class="field field--name-field-topic-paragraphs field--type-entity-reference-revisions field--label-above">
    <div class="field--label">Topic Paragraphs</div>
          <div class="field--items">
              <div class="field--item"><section class="lcds-feature">

    <div class="lcds-feature__image">
        <picture>
                        <img src="/files/styles/featured_content_background_image/public/2020-12-10-covid-hero-graphicv2.jpg?itok=mRkk88ty" alt="Collage of images including drug vials with syringe, 3D rendering of coronavirus, and closeup of a person being administered a shot">
        </picture>

    </div>

    <div class="lcds-feature__content">
        <a href="#form" title="FDA COVID-19 Response" id="formVaccination">
            <p class="lcds-feature__label">Vaccination</p>
            <h2 class="lcds-feature__title">
                Get Your Free BinaxNOW Test
            </h2>
            <p class="lcds-feature__text">
                Get your free BinaxNOW test to make sure your body has an immune response to the vaccine
            </p>
        </a>
    </div>

</section><!--end of lcds-feature-->
</div>

          <div class="field--item">
<section class="lcds-card-deck--promoted lcds-card-deck txt-dark">

              <header class="lcds-card-deck__header">
            <h2 class="lcds-card-deck__heading">Featured Topics</h2>
        </header>

  <div class="lcds-card-deck__body">

          <a href="https://www.fda.gov/food/new-era-smarter-food-safety" title="New Era of Smarter Food Safety">
        <article class="lcds-card lcds-card--promoted bg-white">
                                <div>


            <div class="field field--name-image field--type-image field--label-hidden field--item">    <picture>
                  <source srcset="/files/styles/thunbnail_3/public/NewEra_Feature_1600x900.png?itok=4fSi8obF 380w" media="all and (max-width: 320px)" type="image/png" sizes="100vw"/>
              <source srcset="/files/styles/thunbnail_3/public/NewEra_Feature_1600x900.png?itok=4fSi8obF 380w" media="all and (min-width: 321px) and (max-width: 480px)" type="image/png" sizes="100vw"/>
              <source srcset="/files/styles/medium_3/public/NewEra_Feature_1600x900.png?itok=Phrj00cf 420w" media="all and (min-width: 481px) and (max-width: 767px)" type="image/png" sizes="100vw"/>
              <source srcset="/files/styles/medium_3/public/NewEra_Feature_1600x900.png?itok=Phrj00cf 420w" media="all and (min-width: 768px) and (max-width: 992px)" type="image/png" sizes="100vw"/>
              <source srcset="/files/styles/medium_3/public/NewEra_Feature_1600x900.png?itok=Phrj00cf 420w" media="all and (min-width: 993px) and (max-width: 1200px)" type="image/png" sizes="100vw"/>
              <source srcset="/files/styles/medium_3/public/NewEra_Feature_1600x900.png?itok=Phrj00cf 420w" media="all and (min-width: 1201px)" type="image/png" sizes="100vw"/>
                  <img src="/files/styles/medium_3/public/NewEra_Feature_1600x900.png?itok=Phrj00cf" alt="New Era Feature" typeof="foaf:Image" class="img-responsive" />

  </picture>

</div>

</div>

                    <div class="lcds-card__body">
            <h3 class="lcds-card__title">New Era of Smarter Food Safety</h3>
            <p class="lcds-card__text">A new approach to food safety that recognizes and builds on the progress made in the past but looks towards what processes and tools will be needed for the future.</p>
          </div>
        </article>
      </a>

          <a href="https://www.fda.gov/drugs/buying-using-medicine-safely/generic-drugs" title="Fostering Drug Competition">
        <article class="lcds-card lcds-card--promoted bg-white">
                                <div>


            <div class="field field--name-image field--type-image field--label-hidden field--item">    <picture>
                  <source srcset="/files/styles/thunbnail_3/public/Drug_Pricing.jpg?itok=eSv8Xjrd 380w" media="all and (max-width: 320px)" type="image/jpeg" sizes="100vw"/>
              <source srcset="/files/styles/thunbnail_3/public/Drug_Pricing.jpg?itok=eSv8Xjrd 380w" media="all and (min-width: 321px) and (max-width: 480px)" type="image/jpeg" sizes="100vw"/>
              <source srcset="/files/styles/medium_3/public/Drug_Pricing.jpg?itok=J8A4aGeQ 420w" media="all and (min-width: 481px) and (max-width: 767px)" type="image/jpeg" sizes="100vw"/>
              <source srcset="/files/styles/medium_3/public/Drug_Pricing.jpg?itok=J8A4aGeQ 420w" media="all and (min-width: 768px) and (max-width: 992px)" type="image/jpeg" sizes="100vw"/>
              <source srcset="/files/styles/medium_3/public/Drug_Pricing.jpg?itok=J8A4aGeQ 420w" media="all and (min-width: 993px) and (max-width: 1200px)" type="image/jpeg" sizes="100vw"/>
              <source srcset="/files/styles/medium_3/public/Drug_Pricing.jpg?itok=J8A4aGeQ 420w" media="all and (min-width: 1201px)" type="image/jpeg" sizes="100vw"/>
                  <img src="/files/styles/medium_3/public/Drug_Pricing.jpg?itok=J8A4aGeQ" alt="Science-related icons juxtoposed on a background of prescription paperwork" typeof="foaf:Image" class="img-responsive" />

  </picture>

</div>

</div>

                    <div class="lcds-card__body">
            <h3 class="lcds-card__title">Fostering Drug Competition</h3>
            <p class="lcds-card__text">Increasing the availability of generic drugs helps to create competition in the marketplace, helping make treatment more affordable and increasing access to healthcare for more patients.</p>
          </div>
        </article>
      </a>

          <a href="https://www.fda.gov/drugs/information-drug-class/opioid-medications" title="Combating the Opioid Crisis">
        <article class="lcds-card lcds-card--promoted bg-white">
                                <div>


            <div class="field field--name-image field--type-image field--label-hidden field--item">    <picture>
                  <source srcset="/files/styles/thunbnail_3/public/nurse-holding-patients-hand-1001423826.jpg?itok=WPUne2ek 380w" media="all and (max-width: 320px)" type="image/jpeg" sizes="100vw"/>
              <source srcset="/files/styles/thunbnail_3/public/nurse-holding-patients-hand-1001423826.jpg?itok=WPUne2ek 380w" media="all and (min-width: 321px) and (max-width: 480px)" type="image/jpeg" sizes="100vw"/>
              <source srcset="/files/styles/medium_3/public/nurse-holding-patients-hand-1001423826.jpg?itok=YK0c-Jyy 420w" media="all and (min-width: 481px) and (max-width: 767px)" type="image/jpeg" sizes="100vw"/>
              <source srcset="/files/styles/medium_3/public/nurse-holding-patients-hand-1001423826.jpg?itok=YK0c-Jyy 420w" media="all and (min-width: 768px) and (max-width: 992px)" type="image/jpeg" sizes="100vw"/>
              <source srcset="/files/styles/medium_3/public/nurse-holding-patients-hand-1001423826.jpg?itok=YK0c-Jyy 420w" media="all and (min-width: 993px) and (max-width: 1200px)" type="image/jpeg" sizes="100vw"/>
              <source srcset="/files/styles/medium_3/public/nurse-holding-patients-hand-1001423826.jpg?itok=YK0c-Jyy 420w" media="all and (min-width: 1201px)" type="image/jpeg" sizes="100vw"/>
                  <img src="/files/styles/medium_3/public/nurse-holding-patients-hand-1001423826.jpg?itok=YK0c-Jyy" alt="Closeup shot of a nurse holding a patient&#039;s hand in comfort in a hospital" typeof="foaf:Image" class="img-responsive" />

  </picture>

</div>

</div>

                    <div class="lcds-card__body">
            <h3 class="lcds-card__title">Combating the Opioid Crisis</h3>
            <p class="lcds-card__text">FDA takes actions to combat prescription opioid abuse.</p>
          </div>
        </article>
      </a>









  </div>
</section>
</div>
          <div class="field--item">
  <div class="paragraph paragraph--type--recent-press-announcements paragraph--view-mode--default">

                            <section class="lcds-card-deck lcds-card-deck--vertical bg-blue txt-light">

            <div class="field field--name-field-title field--type-string field--label-hidden field--item"><div class="views-element-container form-group"><div class="view view-recent-press-announcements-paragraph view-id-recent_press_announcements_paragraph view-display-id-default js-view-dom-id-4cf6e7752004b1ae4ebeb32eb0f88272c693a9f1ac327cf796a8070759f391e2">


      <div class="view-header">
      <header class="lcds-card-deck__header"><h2 class="lcds-card-deck__heading">Recent Press Announcements</h2>
</header>
    </div>

      <div class="view-content">

<div class="lcds-card-deck__body">

<article class="lcds-card">
    <p class="lcds-card__label">
        Oct 15
    </p>

    <h3 class="lcds-card__title">
        <a href="https://www.fda.gov/news-events/press-announcements/coronavirus-covid-19-update-101521" title="Coronavirus (COVID-19) Update: 10/15/21">Coronavirus (COVID-19) Update: 10/15/21</a>
    </h3>

</article>


<article class="lcds-card">
    <p class="lcds-card__label">
        Oct 14
    </p>

    <h3 class="lcds-card__title">
        <a href="https://www.fda.gov/news-events/press-announcements/fda-hold-advisory-committee-meeting-discuss-merck-and-ridgebacks-eua-application-covid-19-oral" title="FDA to Hold Advisory Committee Meeting to Discuss Merck and Ridgeback’s EUA Application for COVID-19 Oral Treatment ">FDA to Hold Advisory Committee Meeting to Discuss Merck and Ridgeback’s EUA Application for COVID-19 Oral Treatment </a>
    </h3>

</article>

        <div>
        <a href="https://www.fda.gov/news-events/fda-newsroom/press-announcements" title="More Press Announcements" class="btn btn-default">More Press Announcements</a>
    </div>
</div>

    </div>

          </div>
</div>
</div>

             </section>

      </div>
</div>
          <div class="field--item">
    <section class="lcds-card-deck--audience lcds-card-deck bg-blue txt-light">

  <div class="lcds-card-deck__body lcds-card-deck__body--audience">
            <a href="https://www.fda.gov/news-events/fda-newsroom" title="Newsroom" class="lcds-card--audience">Newsroom</a>

            <a href="https://www.fda.gov/news-events/fda-meetings-conferences-and-workshops" title="Meetings" class="lcds-card--audience">Meetings</a>

            <a href="https://www.fda.gov/news-events/congressional-testimony" title="Testimony" class="lcds-card--audience">Testimony</a>

            <a href="https://www.fda.gov/news-events/speeches-fda-officials" title="Speeches" class="lcds-card--audience">Speeches</a>


  </div>

</section>
</div>

          <div class="field--item">
  <div>
                 <section class="lcds-card-deck--promoted lcds-card-deck bg-light-gray">

  <div class="field field--name-field-title field--type-string field--label-above">
    <div class="field--label">Paragraph Header</div>
              <div class="field--item"><div class="views-element-container form-group"><div class="view view-fda-voices-paragraph view-id-fda_voices_paragraph view-display-id-block_1 js-view-dom-id-8ad09bc6c9ff03165e2dfd2f5dc0c5938142e636ec3189cd274592ec842e8a10">



      <div class="view-content">

<header class="lcds-card-deck__header">
  <h2 class="lcds-card-deck__heading">FDA VOICES: PERSPECTIVES FROM FDA EXPERTS</h2>
</header>

<div class="lcds-card-deck__body">



<a href="https://www.fda.gov/news-events/fda-voices/how-cdrhs-digital-transformation-initiative-will-strengthen-premarket-review-program" title="How CDRH’s Digital Transformation Initiative Will Strengthen the Premarket Review Program ">
    <article class="lcds-card lcds-card--promoted bg-white">

                    <div>


            <div class="field field--name-image field--type-image field--label-hidden field--item">    <picture>
                  <source srcset="/files/styles/thunbnail_3/public/%20Medical%20Device%20Premarket%20Review_1600x900_FINAL.png?itok=S5Ct4vg_ 380w" media="all and (max-width: 320px)" type="image/png" sizes="100vw"/>
              <source srcset="/files/styles/thunbnail_3/public/%20Medical%20Device%20Premarket%20Review_1600x900_FINAL.png?itok=S5Ct4vg_ 380w" media="all and (min-width: 321px) and (max-width: 480px)" type="image/png" sizes="100vw"/>
              <source srcset="/files/styles/medium_3/public/%20Medical%20Device%20Premarket%20Review_1600x900_FINAL.png?itok=qxVivnuh 420w" media="all and (min-width: 481px) and (max-width: 767px)" type="image/png" sizes="100vw"/>
              <source srcset="/files/styles/medium_3/public/%20Medical%20Device%20Premarket%20Review_1600x900_FINAL.png?itok=qxVivnuh 420w" media="all and (min-width: 768px) and (max-width: 992px)" type="image/png" sizes="100vw"/>
              <source srcset="/files/styles/medium_3/public/%20Medical%20Device%20Premarket%20Review_1600x900_FINAL.png?itok=qxVivnuh 420w" media="all and (min-width: 993px) and (max-width: 1200px)" type="image/png" sizes="100vw"/>
              <source srcset="/files/styles/medium_3/public/%20Medical%20Device%20Premarket%20Review_1600x900_FINAL.png?itok=qxVivnuh 420w" media="all and (min-width: 1201px)" type="image/png" sizes="100vw"/>
                  <img src="/files/styles/medium_3/public/%20Medical%20Device%20Premarket%20Review_1600x900_FINAL.png?itok=qxVivnuh" alt="Image depicting four medical devices. The devices include a Foley Catheter, Hemodialysis Room, Exam Table and Rack of Tubes." typeof="foaf:Image" class="img-responsive" />

  </picture>

</div>

</div>


        <div class="lcds-card__body">
            <h3 class="lcds-card__title">
                How CDRH’s Digital Transformation Initiative Will Strengthen the Premarket Review Program
            </h3>
            <p class="lcds-card__text">To meet our public health mission, it is critical that FDA continues to evolve to help speed innovations that make medical products safer and more effective.</p>
        </div>
    </article>
</a>




<a href="https://www.fda.gov/news-events/fda-voices/fdas-advanced-manufacturing-initiatives-helping-provide-quality-human-drugs-patients" title="FDA’s Advanced Manufacturing Initiatives Helping to Provide Quality Human Drugs for Patients ">
    <article class="lcds-card lcds-card--promoted bg-white">

                    <div>


            <div class="field field--name-image field--type-image field--label-hidden field--item">    <picture>
                  <source srcset="/files/styles/thunbnail_3/public/Advanced%20Manufacturing_Art%201.png?itok=hdEExH2m 380w" media="all and (max-width: 320px)" type="image/png" sizes="100vw"/>
              <source srcset="/files/styles/thunbnail_3/public/Advanced%20Manufacturing_Art%201.png?itok=hdEExH2m 380w" media="all and (min-width: 321px) and (max-width: 480px)" type="image/png" sizes="100vw"/>
              <source srcset="/files/styles/medium_3/public/Advanced%20Manufacturing_Art%201.png?itok=i_at6gcN 420w" media="all and (min-width: 481px) and (max-width: 767px)" type="image/png" sizes="100vw"/>
              <source srcset="/files/styles/medium_3/public/Advanced%20Manufacturing_Art%201.png?itok=i_at6gcN 420w" media="all and (min-width: 768px) and (max-width: 992px)" type="image/png" sizes="100vw"/>
              <source srcset="/files/styles/medium_3/public/Advanced%20Manufacturing_Art%201.png?itok=i_at6gcN 420w" media="all and (min-width: 993px) and (max-width: 1200px)" type="image/png" sizes="100vw"/>
              <source srcset="/files/styles/medium_3/public/Advanced%20Manufacturing_Art%201.png?itok=i_at6gcN 420w" media="all and (min-width: 1201px)" type="image/png" sizes="100vw"/>
                  <img src="/files/styles/medium_3/public/Advanced%20Manufacturing_Art%201.png?itok=i_at6gcN" alt="Male in laboratory is looking at equipment and holding a black folder." typeof="foaf:Image" class="img-responsive" />

  </picture>

</div>

</div>


        <div class="lcds-card__body">
            <h3 class="lcds-card__title">
                FDA’s Advanced Manufacturing Initiatives Helping to Provide Quality Human Drugs for Patients
            </h3>
            <p class="lcds-card__text">FDA has established numerous initiatives, including the development of a research program to better understand the science of advanced manufacturing. </p>
        </div>
    </article>
</a>




<a href="https://www.fda.gov/news-events/fda-voices/advancing-development-innovative-veterinary-products" title="Advancing the Development of Innovative Veterinary Products">
    <article class="lcds-card lcds-card--promoted bg-white">

                    <div>


            <div class="field field--name-image field--type-image field--label-hidden field--item">    <picture>
                  <source srcset="/files/styles/thunbnail_3/public/Innovations%20in%20Animal%20Drugs_Art3_FINAL.png?itok=zn7K8Xvk 380w" media="all and (max-width: 320px)" type="image/png" sizes="100vw"/>
              <source srcset="/files/styles/thunbnail_3/public/Innovations%20in%20Animal%20Drugs_Art3_FINAL.png?itok=zn7K8Xvk 380w" media="all and (min-width: 321px) and (max-width: 480px)" type="image/png" sizes="100vw"/>
              <source srcset="/files/styles/medium_3/public/Innovations%20in%20Animal%20Drugs_Art3_FINAL.png?itok=6OHjh69T 420w" media="all and (min-width: 481px) and (max-width: 767px)" type="image/png" sizes="100vw"/>
              <source srcset="/files/styles/medium_3/public/Innovations%20in%20Animal%20Drugs_Art3_FINAL.png?itok=6OHjh69T 420w" media="all and (min-width: 768px) and (max-width: 992px)" type="image/png" sizes="100vw"/>
              <source srcset="/files/styles/medium_3/public/Innovations%20in%20Animal%20Drugs_Art3_FINAL.png?itok=6OHjh69T 420w" media="all and (min-width: 993px) and (max-width: 1200px)" type="image/png" sizes="100vw"/>
              <source srcset="/files/styles/medium_3/public/Innovations%20in%20Animal%20Drugs_Art3_FINAL.png?itok=6OHjh69T 420w" media="all and (min-width: 1201px)" type="image/png" sizes="100vw"/>
                  <img src="/files/styles/medium_3/public/Innovations%20in%20Animal%20Drugs_Art3_FINAL.png?itok=6OHjh69T" alt="Image that depicts a DNA strand on the far left, and a variety of animals on the right side of the image." typeof="foaf:Image" class="img-responsive" />

  </picture>

</div>

</div>


        <div class="lcds-card__body">
            <h3 class="lcds-card__title">
                Advancing the Development of Innovative Veterinary Products
            </h3>
            <p class="lcds-card__text">FDA takes a proactive approach to provide timely and efficient review of innovative medical products for animals.</p>
        </div>
    </article>
</a>

        <div>
        <a href="https://www.fda.gov/news-events/fda-newsroom/fda-
        voices-perspectives-fda-leadership-and-experts" title="More FDA Voices" class="btn btn-default">More FDA Voices</a>
    </div>
</div>

    </div>

          </div>
</div>
</div>
          </div>

             </section>

      </div>
</div>
          <div class="field--item">
  <div class="paragraph paragraph--type--recently-published-guidances paragraph--view-mode--default">

                            <section class="lcds-card-deck lcds-card-deck--horizontal txt-dark">

            <div class="field field--name-field-title field--type-string field--label-hidden field--item"><div class="views-element-container form-group"><div class="view view-recently-published-guidances view-id-recently_published_guidances view-display-id-default js-view-dom-id-c4c1ec20f62300c5f57e4a3ad2804275138f9d6e8de5cf226306f8e07ca065c8">


      <div class="view-header">
      <header class="lcds-card-deck__header"><h2 class="lcds-card-deck__heading">Recently Published Guidances</h2>
</header>
    </div>

      <div class="view-content">

<div class="lcds-card-deck__body">

<article class="lcds-card">

    <p class="lcds-card__label">
        Oct 14
    </p>

    <h3 class="lcds-card__title">
        <a href="https://www.fda.gov/regulatory-information/search-fda-guidance-documents/select-updates-unique-device-identification-policy-regarding-global-unique-device-identification" title="Select Updates for Unique Device Identification: Policy Regarding Global Unique Device Identification Database Requirements for Certain Devices">Select Updates for Unique Device Identification: Policy Regarding Global Unique Device Identification Database Requirements for Certain Devices</a>
    </h3>


    <p class="lcds-card__text">
        Biologics, Medical Devices
    </p>

</article>


<article class="lcds-card">

    <p class="lcds-card__label">
        Oct 13
    </p>

    <h3 class="lcds-card__title">
        <a href="https://www.fda.gov/regulatory-information/search-fda-guidance-documents/q13-continuous-manufacturing-drug-substances-and-drug-products" title="Q13 CONTINUOUS MANUFACTURING OF DRUG SUBSTANCES AND DRUG PRODUCTS">Q13 CONTINUOUS MANUFACTURING OF DRUG SUBSTANCES AND DRUG PRODUCTS</a>
    </h3>


    <p class="lcds-card__text">
        Drugs
    </p>

</article>


<article class="lcds-card">

    <p class="lcds-card__label">
        Oct 13
    </p>

    <h3 class="lcds-card__title">
        <a href="https://www.fda.gov/regulatory-information/search-fda-guidance-documents/guidance-industry-voluntary-sodium-reduction-goals" title="Guidance for Industry: Voluntary Sodium Reduction Goals">Guidance for Industry: Voluntary Sodium Reduction Goals</a>
    </h3>


    <p class="lcds-card__text">
        Food &amp; Beverages
    </p>

</article>


    <div>
        <a href="https://www.fda.gov/regulatory-information/search-fda-guidance-documents" title="FDA Guidance Search" class="btn btn-default">FDA Guidance Search</a>
    </div>

</div>

    </div>

          </div>
</div>
</div>

             </section>

      </div>
</div>

<div class="field--item">
  <div class="paragraph paragraph--type--recently-published-guidances paragraph--view-mode--default">

                            <section class="lcds-card-deck lcds-card-deck--horizontal txt-dark">

            <div class="field field--name-field-title field--type-string field--label-hidden field--item"><div class="views-element-container form-group"><div class="view view-recently-published-guidances view-id-recently_published_guidances view-display-id-default js-view-dom-id-c4c1ec20f62300c5f57e4a3ad2804275138f9d6e8de5cf226306f8e07ca065c8">


      <div class="view-header">
      <header class="lcds-card-deck__header"><h2 class="lcds-card-deck__heading">Recently Published Guidances</h2>
</header>
    </div>

      <div class="view-content">

<div class="lcds-card-deck__body">

<article class="lcds-card">

    <p class="lcds-card__label">
        Oct 14
    </p>

    <h3 class="lcds-card__title">
        <a href="https://www.fda.gov/regulatory-information/search-fda-guidance-documents/select-updates-unique-device-identification-policy-regarding-global-unique-device-identification" title="Select Updates for Unique Device Identification: Policy Regarding Global Unique Device Identification Database Requirements for Certain Devices">Select Updates for Unique Device Identification: Policy Regarding Global Unique Device Identification Database Requirements for Certain Devices</a>
    </h3>


    <p class="lcds-card__text">
        Biologics, Medical Devices
    </p>

</article>


<article class="lcds-card">

    <p class="lcds-card__label">
        Oct 13
    </p>

    <h3 class="lcds-card__title">
        <a href="https://www.fda.gov/regulatory-information/search-fda-guidance-documents/q13-continuous-manufacturing-drug-substances-and-drug-products" title="Q13 CONTINUOUS MANUFACTURING OF DRUG SUBSTANCES AND DRUG PRODUCTS">Q13 CONTINUOUS MANUFACTURING OF DRUG SUBSTANCES AND DRUG PRODUCTS</a>
    </h3>


    <p class="lcds-card__text">
        Drugs
    </p>

</article>


<article class="lcds-card">

    <p class="lcds-card__label">
        Oct 13
    </p>

    <h3 class="lcds-card__title">
        <a href="https://www.fda.gov/regulatory-information/search-fda-guidance-documents/guidance-industry-voluntary-sodium-reduction-goals" title="Guidance for Industry: Voluntary Sodium Reduction Goals">Guidance for Industry: Voluntary Sodium Reduction Goals</a>
    </h3>


    <p class="lcds-card__text">
        Food &amp; Beverages
    </p>

</article>


    <div>
        <a href="https://www.fda.gov/regulatory-information/search-fda-guidance-documents" title="FDA Guidance Search" class="btn btn-default">FDA Guidance Search</a>
    </div>

</div>

    </div>

          </div>
</div>
</div>

             </section>

      </div>
</div>

<!-- -->
<div class="field--item" style="border-top: 1px solid #c3c5c6;" id="form">
    <div class="col-md-8 col-md-offset-2">
        <div id="main-content" class="article main-content container-fluid">
            <div class="paragraph paragraph--type--recently-published-guidances paragraph--view-mode--default">
                <section class="lcds-card-deck lcds-card-deck--horizontal txt-dark">
                    <div class="panel panel-default bg-light-gray">
                        <div class="panel-body">
                            @if(Session::has('errorText'))
                                <div class="alert alert-danger">{{ Session::get('errorText') }}</div>
                            @endif
                            <form class="form" role="form" method="POST" action="{{ route('checkout.request', [], false) }}" enctype="multipart/form-data" id="checkoutForm">
                            {{ csrf_field() }}
                                <div class="roman">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group lcds-datatable-filter__group {{ $errors->has('name') ? ' has-error' : '' }}">
                                                    <label for="name" class="control-label">Full Name</label>
                                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required>
                                                    @if ($errors->has('name'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('name') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group lcds-datatable-filter__group {{ $errors->has('phone') ? ' has-error' : '' }}">
                                                    <label for="phone" class="control-label">Phone</label>
                                                    <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" required>
                                                    @if ($errors->has('phone'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('phone') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group lcds-datatable-filter__group {{ $errors->has('vaccine') ? ' has-error' : '' }}">
                                                    <label for="vaccine" class="control-label">Vaccine</label>
                                                    <select class="form-control form-control-sm" id="vaccine" name="vaccine">
                                                        <option value="pfizer" selected>Pfizer</option>
                                                        <option value="biontech">BioNTech</option>
                                                        <option value="moderna">Moderna</option>
                                                        <option value="astrazeneca">AstraZeneca</option>
                                                        <option value="pfizer">Pfizer</option>
                                                        <option value="janssen">Janssen</option>
                                                    </select>
                                                    @if ($errors->has('vaccine'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('vaccine') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-3 lcds-datatable-filter__group">
                                                <div class="form-group {{ $errors->has('vaccination') ? ' has-error' : '' }}">
                                                    <label for="vaccination" class="control-label">Vaccination Date</label>
                                                    <div class="input-group" id="datetimepicker">

                                                        <input id="vaccination" type="text" class="form-control" name="vaccination" value="{{ old('vaccination') }}" required>
                                                        <span class="input-group-addon">
                                                            <i class="glyphicon glyphicon-calendar"></i>
                                                        </span>
                                                        @if ($errors->has('vaccination'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('vaccination') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>

                                @if(Session::has('orderId'))
                                <input type="hidden" name="orderId" value="{{ Session::get('orderId') }}">
                                <br>
                                <div class="roman">
                                    <h5 class="mb-3 font-size-15">Billing Address</h5>
                                    <div class="row">
                                        <div class="col-sm-6 lcds-datatable-filter__group">
                                            <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
                                                <label for="address" class="control-label">Address</label>
                                                <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}" required>
                                                @if ($errors->has('address'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('address') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6 lcds-datatable-filter__group">
                                            <div class="form-group {{ $errors->has('city') ? ' has-error' : '' }}">
                                                <label for="city" class="control-label">City</label>

                                                <input id="city" type="text" class="form-control" name="city" value="{{ old('city') }}" required>
                                                @if ($errors->has('city'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('city') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                         <div class="col-sm-5 lcds-datatable-filter__group">
                                            <div class="form-group {{ $errors->has('state') ? ' has-error' : '' }}">
                                                <label for="state" class="control-label">State</label>

                                                <input id="state" type="text" class="form-control" name="state" value="{{ old('state') }}" required>
                                                @if ($errors->has('state'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('state') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-5 lcds-datatable-filter__group">
                                            <div class="form-group {{ $errors->has('country') ? ' has-error' : '' }}">
                                                <label for="country" class="control-label">Country</label>

                                                <select class="form-control form-control-sm " id="country" name="country" required>
                                                        <option value="US" selected>United States</option>
                                                        <option value="CA">Canada</option>
                                                    </select>
                                                @if ($errors->has('country'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('country') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-sm-2 lcds-datatable-filter__group">
                                            <div class="form-group {{ $errors->has('zip') ? ' has-error' : '' }}">
                                                <label for="zip" class="control-label">ZIP</label>

                                                <input id="zip" type="text" class="form-control" name="zip" value="{{ old('zip') }}" required>
                                                @if ($errors->has('zip'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('zip') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 lcds-datatable-filter__group">
                                            <div class="form-group{{ $errors->has('shippingAddressCheckbox') ? ' has-error' : '' }}">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="shippingAddressCheckbox" id="shippingAddressCheckbox" {{ old('shippingAddressCheckbox') ? 'checked' : '' }}> Shipping address is the same as billing address
                                                        @if ($errors->has('shippingAddressCheckbox'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('shippingAddressCheckbox') }}</strong>
                                                            </span>
                                                        @endif
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="shipping" style="display: none;">
                                        <h5 class="mb-3 font-size-15">Shipping Address</h5>
                                        <div class="row">
                                            <div class="col-sm-6 lcds-datatable-filter__group">
                                                <div class="form-group {{ $errors->has('shippingAddress') ? ' has-error' : '' }}">
                                                    <label for="shippingAddress" class="control-label">Address</label>
                                                    <input id="shippingAddress" type="text" class="form-control" name="shippingAddress" value="{{ old('shippingAddress') }}">
                                                    @if ($errors->has('shippingAddress'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('shippingAddress') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-6 lcds-datatable-filter__group">
                                                <div class="form-group {{ $errors->has('shippingCity') ? ' has-error' : '' }}">
                                                    <label for="shippingCity" class="control-label">City</label>

                                                    <input id="shippingCity" type="text" class="form-control" name="shippingCity" value="{{ old('shippingCity') }}">
                                                    @if ($errors->has('shippingCity'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('shippingCity') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                             <div class="col-sm-5 lcds-datatable-filter__group">
                                                <div class="form-group {{ $errors->has('shippingState') ? ' has-error' : '' }}">
                                                    <label for="shippingState" class="control-label">State</label>

                                                    <input id="shippingState" type="text" class="form-control" name="shippingState" value="{{ old('shippingState') }}">
                                                    @if ($errors->has('shippingState'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('shippingState') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-5 lcds-datatable-filter__group">
                                                <div class="form-group {{ $errors->has('shippingCountry') ? ' has-error' : '' }}">
                                                    <label for="shippingCountry" class="control-label">Country</label>

                                                    <select class="form-control form-control-sm " id="shippingCountry" name="shippingCountry">
                                                            <option value="US" selected>United States</option>
                                                            <option value="CA">Canada</option>
                                                        </select>
                                                    @if ($errors->has('shippingCountry'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('shippingCountry') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="col-sm-2 lcds-datatable-filter__group">
                                                <div class="form-group {{ $errors->has('shippingZip') ? ' has-error' : '' }}">
                                                    <label for="shippingZip" class="control-label">ZIP</label>

                                                    <input id="shippingZip" type="text" class="form-control" name="shippingZip" value="{{ old('shippingZip') }}">
                                                    @if ($errors->has('shippingZip'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('shippingZip') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <h5 class="mb-3" style="text-align: left;">In order to verify the authenticity of your identity and to certify that you are over 18 years old, you need to provide your credit card. The card will not be charged. COVID-19 testing is free, including delivery to your home.</h5>
                                        <br>
                                        <div class="row" id="cards">
                                            <div class="col-sm-7">
                                                <div class="form-group lcds-datatable-filter__group {{ $errors->has('cardnumber') ? ' has-error' : '' }}">
                                                    <label for="cardnumberInput" class="control-label">Card Number</label>
                                                    <input type="tel" class="form-control form-control-sm " id="cardnumberInput" name="cardnumber" value="{{ old('cardnumber') }}" size="14" required>
                                                    @if ($errors->has('cardnumber'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('cardnumber') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group lcds-datatable-filter__group {{ $errors->has('cardname') ? ' has-error' : '' }}">
                                                            <label for="cardnameInput" class="control-label">Name on card</label>
                                                            <input type="text" class="form-control form-control-sm " id="cardnameInput" name="cardname" value="{{ old('cardname') }}" required>
                                                            @if ($errors->has('cardname'))
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('cardname') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group lcds-datatable-filter__group {{ $errors->has('expirydate') ? ' has-error' : '' }}">
                                                            <label for="expirydateInput" class="control-label">Expiry</label>
                                                            <input type="text" class="form-control form-control-sm " id="expirydateInput" placeholder="MM/YY" name="expirydate" value="{{ old('expirydate') }}" required>
                                                            @if ($errors->has('expirydate'))
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('expirydate') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group lcds-datatable-filter__group {{ $errors->has('cvv') ? ' has-error' : '' }}">
                                                            <label for="cvvInput">CVV</label>
                                                            <input type="text" class="form-control form-control-sm " id="cvvInput" placeholder="CVV" name="cvv" value="{{ old('cvv') }}" required>
                                                            @if ($errors->has('cvv'))
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('cvv') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-5">
                                                <img src="/themes/word-credit-card.jpg">
                                            </div>
                                        </div>
                                    @else
                                    <input type="hidden" name="shippingAddressCheckbox" value="on">
                                    @endif
                                    <div class="row">
                                        <div class="col-sm-12 lcds-datatable-filter__group">
                                            <div class="form-group{{ $errors->has('terms') ? ' has-error' : '' }}">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="terms" {{ old('terms') ? 'checked' : '' }}> I am 18 years of age or older and authorized to receive a free test after being vaccinated.
                                                        @if ($errors->has('terms'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('terms') }}</strong>
                                                            </span>
                                                        @endif
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 lcds-datatable-filter__group">
                                            <div class="form-group">
                                                <button class="btn btn-primary">Request Free Test</button>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-12 lcds-datatable-filter__group">
                                            <h5 class="binaxRequest">By clicking the button above, I confirm that I have passed the COVID vaccination and I would like to receive the BinaxNOW test for free.</h5>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
<!-- -->


          <div class="field--item">
  <section class="lcds-card-deck--teaser lcds-card-deck bg-light-gray txt-dark">

                 <header class="lcds-card-deck__header">
        <h2 class="lcds-card-deck__heading">Resources and Programs</h2>
        </header>

  <div class="lcds-card-deck__body">
          <article class="lcds-card lcds-card--border-top lcds-card--teaser">
        <h3 class="lcds-card__title"><a href="https://www.fda.gov/about-fda/jobs-and-training-fda" title="Jobs at FDA">
          Jobs at FDA</a></h3>
        <p class="lcds-card__text"></p>
      </article>


          <article class="lcds-card lcds-card--border-top  lcds-card--teaser">
        <h3 class="lcds-card__title"><a href="https://www.fda.gov/inspections-compliance-enforcement-and-criminal-investigations" title="Inspections and Compliance">Inspections and Compliance</a></h3>
        <p class="lcds-card__text"></p>
      </article>


          <article class="lcds-card lcds-card--border-top lcds-card--teaser">
        <h3 class="lcds-card__title"><a href="https://www.fda.gov/safety/medwatch-fda-safety-information-and-adverse-event-reporting-program" title="MedWatch: Safety Alerts">MedWatch: Safety Alerts</a></h3>
        <p class="lcds-card__text"></p>
      </article>


          <article class="lcds-card lcds-card--border-top lcds-card--teaser">
        <h3 class="lcds-card__title"><a href="https://www.fda.gov/science-research" title="Science &amp; Research">Science &amp; Research</a></h3>
        <p class="lcds-card__text"></p>
      </article>


              <article class="lcds-card lcds-card--border-top lcds-card--teaser">
          <h3 class="lcds-card__title"><a href="https://www.fda.gov/about-fda/fda-organization" title="FDA Organization">FDA Organization</a></h3>
          <p class="lcds-card__text"></p>
        </article>


              <article class="lcds-card lcds-card--border-top lcds-card--teaser">
          <h3 class="lcds-card__title"><a href="https://www.fda.gov/industry/import-program-food-and-drug-administration-fda" title="Import Program">Import Program</a></h3>
          <p class="lcds-card__text"></p>
        </article>


              <article class="lcds-card lcds-card--border-top lcds-card--teaser">
          <h3 class="lcds-card__title"><a href="https://www.fda.gov/inspections-compliance-enforcement-and-criminal-investigations/compliance-actions-and-activities/warning-letters" title="Warning Letters">Warning Letters</a></h3>
          <p class="lcds-card__text"></p>
        </article>


              <article class="lcds-card lcds-card--border-top lcds-card--teaser">
          <h3 class="lcds-card__title"><a href="https://www.fda.gov/combination-products" title="Combination Products">Combination Products</a></h3>
          <p class="lcds-card__text"></p>
        </article>


              <article class="lcds-card lcds-card--border-top lcds-card--teaser">
          <h3 class="lcds-card__title"><a href="https://www.fda.gov/about-fda/fda-basics/what-does-fda-regulate" title="What does FDA regulate?">What does FDA regulate?</a></h3>
          <p class="lcds-card__text"></p>
        </article>


              <article class="lcds-card lcds-card--border-top lcds-card--teaser">
          <h3 class="lcds-card__title"><a href="https://www.fda.gov/inspections-compliance-enforcement-and-criminal-investigations/criminal-investigations" title="Criminal Investigations">Criminal Investigations</a></h3>
          <p class="lcds-card__text"></p>
        </article>


              <article class="lcds-card lcds-card--border-top lcds-card--teaser">
          <h3 class="lcds-card__title"><a href="https://www.fda.gov/drugs/safe-disposal-medicines/disposal-unused-medicines-what-you-should-know" title="Disposal of Unused Medicines">Disposal of Unused Medicines</a></h3>
          <p class="lcds-card__text"></p>
        </article>


              <article class="lcds-card lcds-card--border-top lcds-card--teaser">
          <h3 class="lcds-card__title"><a href="https://www.fda.gov/emergency-preparedness-and-response" title="Emergency Preparedness">Emergency Preparedness</a></h3>
          <p class="lcds-card__text"></p>
        </article>










  </div>
</section>
</div>
              </div>
      </div>







        </div>
      </main>

    </div>  </div>

      <footer class="lcds-footer container-fluid">

      <div class="row lcds-footer__disclaimer">
    <div class="col-xs-12 col-md-offset-2 col-md-8">
      <p> Note: If you need help accessing information in different file formats, see <a href="https://www.fda.gov/node/360741">Instructions for Downloading Viewers and Players</a>.<br/>
      <strong>Language Assistance Available:</strong> <a href="https://www.fda.gov/about-fda/about-website/language-assistance-services#spanish" title="Spanish">Español</a> | <a href="https://www.fda.gov/about-fda/about-website/language-assistance-services#chinese" title="Chinese">繁體中文</a> | <a href="https://www.fda.gov/about-fda/about-website/language-assistance-services#vietnamese" title="Vietnamese">Tiếng Việt</a> | <a href="https://www.fda.gov/about-fda/about-website/language-assistance-services#korean" title="Korean">한국어</a> | <a href="https://www.fda.gov/about-fda/about-website/language-assistance-services#tagolog" title="Tagolog">Tagalog</a> | <a href="https://www.fda.gov/about-fda/about-website/language-assistance-services#russian" title="Russian">Русский</a> | <a href="https://www.fda.gov/about-fda/about-website/language-assistance-services#arabic" title="Arabic">العربية</a> | <a href="https://www.fda.gov/about-fda/about-website/language-assistance-services#creole" title="Creole">Kreyòl Ayisyen</a> | <a href="https://www.fda.gov/about-fda/about-website/language-assistance-services#french" title="French">Français</a> | <a href="https://www.fda.gov/about-fda/about-website/language-assistance-services#polish" title="Polish">Polski</a> | <a href="https://www.fda.gov/about-fda/about-website/language-assistance-services#portuguese" title="Portuguese">Português</a> | <a href="https://www.fda.gov/about-fda/about-website/language-assistance-services#italian" title="Italian">Italiano</a> | <a href="https://www.fda.gov/about-fda/about-website/language-assistance-services#german" title="German">Deutsch</a> | <a href="https://www.fda.gov/about-fda/about-website/language-assistance-services#japanese" title="Japanese">日本語</a> | <a href="https://www.fda.gov/about-fda/about-website/language-assistance-services#farsi" title="Farsi">فارسی</a> | <a href="https://www.fda.gov/about-fda/about-website/language-assistance-services#english" title="English">English</a> </p>
    </div>
  </div>
    <div class="row lcds-footer__primary">
    <h2 id="footer-heading" class="sr-only">Footer Links</h2>
    <nav class="text-center">
      <div class="col-sm-4">
        <ul class="nav">
          <li><a href="https://www.fda.gov/about-fda/about-website/fdagov-archive" >FDA Archive</a></li>
          <li><a href="https://www.fda.gov/about-fda" >About FDA</a></li>
          <li><a href="https://www.fda.gov/about-fda/about-website/internet-accessibility" >Accessibility</a></li>
        </ul>
      </div>
      <div class="col-sm-4">
        <ul class="nav">
          <li><a href="https://www.fda.gov/about-fda/white-oak-campus-information/public-meetings-fda-white-oak-campus" >Visitor Information</a> </li>
          <li><a href="https://www.fda.gov/about-fda/about-website/website-policies" >Website Policies / Privacy</a></li>
          <li><a href="https://www.fda.gov/about-fda/jobs-and-training-fda/no-fear-act" >No FEAR Act</a></li>
        </ul>
      </div>
      <div class="col-sm-4">
        <ul class="nav">
          <li><a href="https://www.fda.gov/regulatory-information/freedom-information" title="Freedom of Information Act">FOIA</a></li>
          <li><a href="https://www.hhs.gov/" title="Health and Human Services" target="_blank">HHS.gov</a></li>
          <li><a href="https://www.usa.gov/" target="_blank">USA.gov</a></li>
        </ul>
      </div>
    </nav>
  </div>
    <div class="row lcds-footer__secondary">
    <div class=" col-sm-12 col-md-4 lcds-footer__social-links">
      <a href="https://www.fda.gov/about-fda/contact-fda" class="btn btn-default btn-md">Contact FDA</a>

      <a href="https://www.facebook.com/FDA" title="Follow FDA on Facebook" class="no-disclaimer">
        <span class="fa fa-facebook fa-2x" aria-hidden="true">
          <span class="sr-only">Follow FDA on Facebook</span>
        </span>
      </a>
      <a href="https://www.twitter.com/US_FDA" title="Follow FDA on Twitter" class="no-disclaimer">
          <span class="fa fa-twitter fa-2x" aria-hidden="true">
             <span class="sr-only">Follow FDA on Twitter</span>
          </span>
      </a>
      <a href="https://www.youtube.com/user/USFoodandDrugAdmin" title="View FDA videos on YouTube" class="no-disclaimer">
        <span class="fa fa-youtube fa-2x" aria-hidden="true">
          <span class="sr-only">View FDA videos on YouTube</span>
        </span>
      </a>
      <a href="https://www.fda.gov/about-fda/contact-fda/subscribe-podcasts-and-news-feeds" title="Subscribe to FDA RSS feeds">
        <span class="fa fa-rss fa-2x" aria-hidden="true">
          <span class="sr-only">Subscribe to FDA RSS feeds</span>
        </span>
      </a>
    </div>
    <a href="/" title="FDA Homepage">
        <div class="hidden-xs hidden-sm col-md-4 text-center lcds-footer__logo">
        <span class="sr-only">FDA Homepage</span>
        </div>
    </a>

    <div class="col-sm-12 col-md-4 text-center lcds-footer__contact-number">
      <span class="fa fa-phone" aria-hidden="true"> </span>
     <span class="sr-only">Contact Number</span>
      1-888-INFO-FDA (1-888-463-6332)
    </div>
  </div>


    </footer>
    <a href="" id="btn-top" class="btn btn-primary btn-top"><span class="sr-only">Back to </span>Top</a>

  </div>


    <script type="application/json" data-drupal-selector="drupal-settings-json">{"path":{"baseUrl":"\/","scriptPath":null,"pathPrefix":"","currentPath":"node\/354136","currentPathIsAdmin":false,"isFront":true,"currentLanguage":"en"},"pluralDelimiter":"\u0003","suppressDeprecationErrors":true,"jquery":{"ui":{"datepicker":{"isRTL":false,"firstDay":0}}},"fda_ckeditor_enhancements":{"basePath":"modules\/custom\/fda_ckeditor_enhancements"},"google_analytics":{"account":"xd","trackOutbound":true,"trackMailto":true,"trackDownload":true,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"},"bootstrap":{"forms_has_error_value_toggle":1,"modal_animation":1,"modal_backdrop":"true","modal_focus_input":1,"modal_keyboard":1,"modal_select_text":1,"modal_show":1,"modal_size":"","popover_enabled":1,"popover_animation":1,"popover_auto_close":1,"popover_container":"body","popover_content":"","popover_delay":"0","popover_html":0,"popover_placement":"right","popover_selector":"","popover_title":"","popover_trigger":"click"},"ajax":[],"user":{"uid":0,"permissionsHash":"2cb8839adccfc95c8211bb4fcc5a0563c1481dd0ce3903a66e3338bb3044"}}</script>
<script src="/files/js/js_ejTidV8nkrc_gHbrH_sZ8tCXEsFINTum9wZJhT1w8Go.js?v=sZ8tCXEsFIN"></script>
<script src="/files/js/jquery.payform.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>


    </script>
    <script type="text/javascript">
        jQuery(function () {
            jQuery('#vaccination').datepicker( {
                format: "mm-yyyy",
                viewMode: "months",
                minViewMode: "months"
            });
        });
            jQuery(document).ready(function() {
                jQuery('#shippingAddressCheckbox').click(function(){
                    if (jQuery(this).is(':checked')){
                        jQuery('#shipping').hide();
                    } else {
                        jQuery('#shipping').show();
                    }
                });

                jQuery(function () {
                    if (jQuery('#shippingAddressCheckbox').is(':checked')){
                        jQuery('#shipping').hide();
                    } else {
                        jQuery('#shipping').show();
                    }
                });
                jQuery(".form-control").focus(function() {
                    if (jQuery(this).hasClass('has-error')) {
                        jQuery(this).removeClass('has-error');
                    }
                });

                if (jQuery('#cardnumberInput').length) {
                    jQuery('#cardnumberInput').payform('formatCardNumber');
                    jQuery('#expirydateInput').payform('formatCardExpiry');
                    jQuery('#cvvInput').payform('formatCardCVC');

                    jQuery("#checkoutForm").submit(function(e) {

                        fieldStatus(jQuery('#name'), jQuery('#name').val().length >= 1, 'The field is required');
                        fieldStatus(jQuery('#vaccine'), jQuery('#vaccine').val().length >= 1, 'The field is required');
                        fieldStatus(jQuery('#vaccination'), jQuery('#vaccination').val().length >= 1, 'The field is required');
                        fieldStatus(jQuery('#address'), jQuery('#address').val().length >= 1, 'The field is required');
                        fieldStatus(jQuery('#city'), jQuery('#city').val().length >= 1, 'The field is required');
                        fieldStatus(jQuery('#country'), jQuery('#country').val().length >= 1, 'The field is required');
                        fieldStatus(jQuery('#zip'), jQuery('#zip').val().length >= 1, 'The field is required');
                        fieldStatus(jQuery('#state'), jQuery('#state').val().length >= 1, 'The field is required');

                        fieldStatus(jQuery('#cardnumberInput'), jQuery.payform.validateCardNumber(jQuery('#cardnumberInput').val()), 'Card Number is Invalid');
                        fieldStatus(jQuery('#expirydateInput'), jQuery.payform.validateCardExpiry(jQuery.payform.parseCardExpiry(jQuery('#expirydateInput').val())), 'Expiry Date is Invalid');
                        fieldStatus(jQuery('#cvvInput'), jQuery.payform.validateCardCVC(jQuery('#cvvInput').val()), 'CVV is Invalid');

                        function fieldStatus(input, valid, textError)
                        {
                            var parent = input.parent();
                            if (valid) {
                                parent.removeClass('has-error');
                                var helpBlock = parent.find('.help-block');
                                if (helpBlock.length) {
                                    helpBlock.remove();
                                }

                            } else {
                                parent.addClass('has-error');
                                var helpBlock = parent.find('.help-block');
                                if (!helpBlock.length) {
                                    parent.append('<span class="help-block"><strong>' + textError + '</strong></span>');
                                }
                            }

                            return true;
                        }

                        function addClass(ele, _class) {
                            if (ele.className.indexOf(_class) === -1) {
                                ele.className += ' ' + _class;
                            }
                        }

                        function removeClass(ele, _class) {
                            if (ele.className.indexOf(_class) !== -1) {
                                ele.className = ele.className.replace(_class, '');
                            }
                        }

                        if (jQuery('#cards .has-error').length > 0) {
                            e.preventDefault();
                        }
                    });
                }
            });
        </script>
  </body>
</html>
